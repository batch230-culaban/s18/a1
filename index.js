// Fucntion without Return

function numAdd (num1, num2){
    let totalSum = num1 + num2;
    console.log("Displayed sum of 5 and 15");
    console.log(totalSum);
}

numAdd(5, 15);

function numSubtract (num1, num2){
    let totalDifference = num1 - num2;
    console.log("Displayed difference of 20 and 5");
    console.log(totalDifference);
}

numSubtract(20, 5);

// End of Function without Return

// Function with Return

function returnProduct(num1, num2){
    console.log("The product of 50 and 10:");
    let theProduct = num1 * num2;
    return theProduct;
}
    let totalProduct = returnProduct(50, 10);
    console.log(totalProduct);

function returnQuotient(num1, num2){
    console.log("The quotient of 50 and 10:");
    let theQuotient = num1 / num2;
    return theQuotient;
}
    let totalQuotient = returnQuotient(50, 10);
    console.log(totalQuotient);

// Area of a Circle

function returnArea(radius){
    console.log("The result of getting the area of a circle with 15 radius:");
    const pi = 3.1416;
    let theArea = pi * ((radius) ** 2);
    return theArea;
}
    let computeCircleArea = returnArea(15);
    console.log(computeCircleArea);

// Total Average of Four Numbers

function returnAverage(num1, num2, num3, num4){
    console.log("The average of 20, 40, 60, 80:");
    let theAverage = (num1+num2+num3+num4)/4;
    return theAverage;
}
    let computeAverageVar = returnAverage(20, 40, 60, 80);
    console.log(computeAverageVar);

// Passing Score Percentage

function returnPercentage(score, totalScore){
    console.log("Is 38/50 a passing score?");
    let scorePercent = ((score/totalScore)*100) >= 75;
    return scorePercent;
}

    let isPassingScore = returnPercentage(38, 50);
    console.log(isPassingScore);

// End of Activity
